
-- DIO, EIO, FIO Class
function NumtoBits(num, bits)
    local t={}
    local b, rest
    local bits = bits
    for b=0,bits-1 do
        rest=math.fmod(num, 2)
        t[b]=rest
        num=(num-rest)/2
    end
    if num==0 then
        return t
    else
        print("Not Enough bits to represent this number")
    end

end

-- DIO general Class -DIO
DIO={}

function DIO:new(o, IOAdress, StateAdress, PinNumber, Bits)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.IOAdress = IOAdress or 0
    o.StateAdress = StateAdress or 0
    o.StateNumber = StateNumber or 0
    o.Bits = Bits or 0
    return o
end

function DIO:setState(Value)
    MB.W(self.IOAdress, 0, Value)
end


function DIO:getState()
    local States = MB.R(self.StateAdress, 0)
    local Bits = NumtoBits(States, self.Bits)
    --print(self.IOAdress, self.PinNumber, self.Bits)
    --print(table.concat(bits))
    return Bits[self.PinNumber]
end

function DIO:isStateChanged()
    local CurrentState = self:getState()
    if (self.State == nil) then 
        self.State = CurrentState
        return -1
    end

    if (self.State ~= CurrentState) then
        self.State = CurrentState
	      return CurrentState    
    else
        return -1
    end
end 




EIO=DIO:new()

function EIO:new(o, PinNumber)
    o = o or DIO:new()
    setmetatable(o, self)
    self.__index = self
    o.PinNumber = PinNumber
    o.IOAdress = 2008 + o.PinNumber
    o.StateAdress = 2501
    o.Bits = 8
    return o
end

FIO=DIO:new()

function FIO:new(o, PinNumber)
    o = o or DIO:new()
    setmetatable(o, self)
    self.__index = self
    o.PinNumber = PinNumber
    o.IOAdress = 2000 + o.PinNumber
    o.StateAdress = 2500
    o.Bits = 8
    return o
end




-- CCG Class
CCG = {}

function CCG:new(o, OutPutPin)
    o = o or {}
    setmetatable(o, self)
    self.__index = self
    o.OutPutPin = OutPutPin
    print(o.OutPutPin)

    return o
end

function CCG:setSwitchPin(SwitchPin)
    self.SwitchPin = SwitchPin
    self.SwitchPin:setState(0)

end

function CCG:setExternalSwitchPin(ExternalSwitchPin)
    self.ExternalSwitchPin = ExternalSwitchPin
end

function CCG:setVolageTrigger(VolageTrigger)
    self.VolageTrigger = VolageTrigger
end



function CCG:setTorrTrigger(TorrTrigger)
    local ConvertedVolageTrigger = 10.3+0.8*math.log(TorrTrigger)/math.log(10)
    self.VolageTrigger = ConvertedVolageTrigger
    print(string.format("Set Triger to %.5f Torr (%.5f Volts)", TorrTrigger, ConvertedVolageTrigger))
end

function CCG:getOutPutValue()
    local readout = MB.R(self.OutPutPin, 3)
    -- print(self.OutPutPin, readout)
    return readout
end

function CCG:TurnOffSwitch()
    self.SwitchPin:setState(0)
end

function CCG:TurnOnSwitch()
    self.SwitchPin:setState(1) 
end


-- it's very wierd that after reading, it will write 0 to the switch
-- in order to prevent that problem, we need to restore the "read out value after reading"
function CCG:getSwitchStatus()
    local readout = self.SwitchPin:getState()
    return  readout
end


function CCG:check()
    local OutPutValue = self:getOutPutValue()
    local SwitchStatus = self:getSwitchStatus()
    local ExternalSwitchChangeFlag = self.ExternalSwitchPin:isStateChanged()
    print(string.format("OPin: %d, OValue: %.2f, SPin: %d, SValue, %d", self.OutPutPin, OutPutValue, self.SwitchPin.IOAdress, SwitchStatus))
    -- print(ExternalSwitchChangeFlag)
    if(ExternalSwitchChangeFlag==0) then
        if(SwitchStatus==0) then
            print("Turned on the switch")
            self:TurnOnSwitch()
        else
            print("Turned off the switch")
            self:TurnOffSwitch()
        end

    end

    if (OutPutValue > self.VolageTrigger and SwitchStatus ==1) then
        print("Overload pressure, turned off the switch")
        self:TurnOffSwitch()
    end

end



-- Configure CCGs and their Switches


-- Output Pin Address
AIN13 = 26
AIN12 = 24
AIN11 = 22
AIN10 = 20
AIN9 = 18
AIN8 = 16

-- Power Supply Switches
S6 = EIO:new(nil, 6)
S1 = EIO:new(nil, 1)
S2 = EIO:new(nil, 2)
S3 = EIO:new(nil, 3)
S4 = EIO:new(nil, 4)
S5 = EIO:new(nil, 5)

-- External Switches, they are attached to FIO
FIO7 = FIO:new(nil, 7)
FIO6 = FIO:new(nil, 6)
FIO5 = FIO:new(nil, 5)
FIO4 = FIO:new(nil, 4)
FIO3 = FIO:new(nil, 3)
FIO2 = FIO:new(nil, 2)



-- S0 doesn't work normally, so we need to use S6 for CCG0
CCG0=CCG:new(nil, AIN13)
CCG0:setSwitchPin(S6)
CCG0:setExternalSwitchPin(FIO7)
CCG0:setTorrTrigger(0.001)

CCG1=CCG:new(nil, AIN12)
CCG1:setSwitchPin(S1)
CCG1:setExternalSwitchPin(FIO6)
CCG1:setTorrTrigger(0.001)

CCG2=CCG:new(nil, AIN11)
CCG2:setSwitchPin(S2)
CCG2:setExternalSwitchPin(FIO5)
CCG2:setTorrTrigger(0.001)

CCG3=CCG:new(nil, AIN10)
CCG3:setSwitchPin(S3)
CCG3:setExternalSwitchPin(FIO4)
CCG3:setTorrTrigger(0.001)


CCG4=CCG:new(nil, AIN9)
CCG4:setSwitchPin(S4)
CCG4:setExternalSwitchPin(FIO3)
CCG4:setTorrTrigger(0.001)

CCG5=CCG:new(nil, AIN8)
CCG5:setSwitchPin(S5)
CCG5:setExternalSwitchPin(FIO2)
CCG5:setTorrTrigger(0.001)

CCGS = {CCG0, CCG1, CCG2, CCG3, CCG4, CCG5}





-- print("running")
LJ.IntervalConfig(0, 10)      --set interval to 1000 for 1000ms
local checkInterval=LJ.CheckInterval

while true do
    if LJ.CheckInterval(0) then
        -- CCG0:check()
        print("\nNew round!!")
        for k, v in pairs(CCGS) do
            v:check()
        end
    end
    -- due to the small memory in the labjack
    -- sometimes we will have "not enough memory"
    -- so we should clean unnecessry memory
    collectgarbage()
end
